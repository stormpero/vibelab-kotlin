package com.example.activiti

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val name = findViewById<EditText>(R.id.editTextTextPersonName);
        val surname = findViewById<EditText>(R.id.editTextTextPersonSurname);
        val patronymic = findViewById<EditText>(R.id.editTextTextPersonPatronymic);
        val age = findViewById<EditText>(R.id.editTextTextPersonAge);
        val hobby = findViewById<EditText>(R.id.editTextTextPersonHobby);

        val button = findViewById<Button>(R.id.button);
        button.setOnClickListener {
            Intent(this, InfoActivity::class.java).also {
                it.putExtra("name", name.text.toString())
                it.putExtra("surname", surname.text.toString())
                it.putExtra("patronymic", patronymic.text.toString())
                it.putExtra("age", age.text.toString())
                it.putExtra("hobby", hobby.text.toString())
                startActivity(it)
                finish()
            }
        }
    }
}