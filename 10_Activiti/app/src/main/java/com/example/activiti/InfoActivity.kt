package com.example.activiti

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class InfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        val textView = findViewById<TextView>(R.id.textView);
        val name = intent.getStringExtra("name")
        val surname = intent.getStringExtra("surname")
        val patronymic = intent.getStringExtra("patronymic")
        val age = intent.getStringExtra("age")?.toIntOrNull() ?: 0
        val hobby = intent.getStringExtra("hobby")

        val text = "Имя: $name\n" +
                "Фамилия: $surname\n" +
                "Отчество: $patronymic!\n" +
                "Хобби: $hobby\n" + when (age) {
            in 0..18 -> "Вы молоды, Ваш возраст $age"
            in 18..35 -> "Вы уже взрослый! Вам $age"
            in 60..90 -> "Начинается старческий период! Вам $age"
            else -> "Вам $age, Вы Долгожитель!!"
        }
        textView.text = text
    }
}