package com.example.tableview

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btn = findViewById<Button>(R.id.button)
        val imgView = findViewById<ImageView>(R.id.imageView)
        btn.setOnClickListener {
            btn.setBackgroundColor(Color.rgb((0..255).random(), (0..255).random(), (0..255).random()))

            when((1..5).random()) {
                1 -> imgView.setImageResource(R.drawable.img1)
                2 -> imgView.setImageResource(R.drawable.img2)
                3 -> imgView.setImageResource(R.drawable.img3)
                4 -> imgView.setImageResource(R.drawable.img4)
                5 -> imgView.setImageResource(R.drawable.img5)
            }
        }


    }
}